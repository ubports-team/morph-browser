Source: morph-browser
Section: x11
Priority: optional
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders: Mike Gabriel <sunweaver@debian.org>,
           Marius Gripsgard <marius@debian.org>,
Build-Depends: apparmor:native,
               cmake,
#               apparmor-easyprof,
               debhelper-compat (= 13),
#               dh-apparmor,
               libapparmor-dev,
               libqt5sql5-sqlite,
               lsb-release,
               pkg-config,
               python3-all:any,
               python3-flake8 (>= 2.2.2-1ubuntu4) | python3-flake8:native,
               qml-module-qt-labs-folderlistmodel,
               qml-module-qt-labs-settings,
               qml-module-qtquick2 (>= 5.4),
               qml-module-qtquick-controls2,
               qml-module-qtquick-layouts,
               qml-module-qtsysteminfo,
               qml-module-qttest,
               qml-module-qtwebengine,
               qml-module-lomiri-components,
               qml-module-lomiri-test,
               qml-module-lomiri-components-extras,
               qtbase5-dev (>= 5.4),
               qtbase5-dev-tools,
               qtdeclarative5-dev,
               qttools5-dev-tools,
               qtwebengine5-dev,
               xauth,
               xvfb,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://gitlab.com/ubports/development/core/morph-browser
Vcs-Browser: https://salsa.debian.org/ubports-team/morph-browser
Vcs-Git: https://salsa.debian.org/ubports-team/morph-browser.git

Package: morph-browser
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         fonts-liberation,
         libqt5sql5-sqlite,
         libqt5webengine5,
         qml-module-qt-labs-folderlistmodel,
         qml-module-qt-labs-settings,
         qml-module-qtquick2 (>= 5.5),
         qml-module-qtquick-controls2,
         qml-module-qtquick-layouts,
         qml-module-qtquick-window2 (>= 5.3),
         qml-module-qtsysteminfo,
         qml-module-morph-web (= ${binary:Version}),
         qml-module-lomiri-content,
         qml-module-lomiri-components-extras,
         qml-module-lomiri-components,
         qml-module-lomiri-action,
Provides: www-browser
Description: Web Browser for Lomiri
 Lightweight web browser tailored for Lomiri, based on the Qt WebEngine
 and using the Lomiri UI components.

Package: morph-webapp-container
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         fonts-liberation,
         libqt5sql5-sqlite,
         libqt5webengine5,
         qml-module-qtquick2 (>= 5.5),
         qml-module-qtquick-window2 (>= 5.3),
         qml-module-sso-onlineaccounts,
         qml-module-morph-web (= ${binary:Version}),
         qml-module-lomiri-content,
         qml-module-lomiri-components,
         qml-module-lomiri-action,
         qml-module-lomiri-onlineaccounts-client,
         morph-browser (= ${binary:Version}),
Description: Morph web applications container
 Lightweight webapp container tailored for Lomiri, based on the
 Qt WebEngine and using the Lomiri UI components.

Package: qml-module-morph-web
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends},
         qml-module-qtwebengine,
         qml-module-qtquick2 (>= 5.4),
         qml-module-qtquick-window2 (>= 5.3),
         qml-module-lomiri-components,
Description: Morph web QML module
 Standalone QML module that contains the WebView component,
 in the Morph.Web namespace.

Package: qml-module-morph-web-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Morph web QML module (HTML documentation)
 A standalone QML module that contains the WebView component,
 in the Morph.Web namespace. This package contains the public
 HTML documentation.
